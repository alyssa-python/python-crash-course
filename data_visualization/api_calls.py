import requests

# Make an API call and store the response.
url = 'https://api.airtable.com/v0/appXXXxXX00x0xXXX/Accounts'
headers = { 'Authorization': 'Bearer YOUR_API_KEY' }
r = requests.get(url, headers=headers)
# print(f"Status code: {r.status_code}")
# Store API response in a variable.
response_dict = r.json()

# Process results.
# print(response_dict.keys())
print(f"Got {len(response_dict['records'])} account(s):\n")

# print(response_dict['records'][0].keys())
for account in response_dict['records']:
    print(f"{account['id']} - {account['fields']['Name']}")

print("\n")